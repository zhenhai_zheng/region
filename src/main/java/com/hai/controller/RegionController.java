package com.hai.controller;


import com.hai.pojo.output.RegionOutput;
import com.hai.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 行政区字典表 前端控制器
 * </p>
 *
 * @author zhenhai.zheng
 * @since 2017年10月11日 09:26:28
 */
@RestController
public class RegionController {

    @Autowired
    private RegionService regionService;

    /**
     * 获取行政区域列表
     * @param parentId 父id
     * @param level 级别
     * @return
     */
    @GetMapping(value = "/regions")
    public RegionOutput getRegion(Integer parentId, Integer level) {
        return regionService.getListForRegion(parentId, level);
    }
}
