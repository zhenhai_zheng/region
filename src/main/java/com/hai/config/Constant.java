package com.hai.config;


/**
 * <p>
 *  系统常量定义
 * </p>
 *
 * @author 透云-中软-西安项目组
 * @since 2017-07-19
 */
public interface Constant {
    /**
     * code定义
     */
    interface CodeConfig{
        /**
         * 失败
         */
        public static final Integer CODE_FAILURE = 0;
        /**
         * 成功
         */
        public static final Integer CODE_SUCCESS = 1;
        /**
         * 不允许为空
         */
        public static final Integer CODE_NOT_EMPTY = 2;
        /**
         * 系统异常
         */
        public static final Integer CODE_SYSTEM_EXCEPTION = 3;
        /**
         * 未查找到结果
         */
        public static final Integer CODE_NOT_FOUND_RESULT = 4;
    }
    /**
     * Message定义
     */
    interface MessageConfig{
        /**
         * 失败
         */
        public static final String MSG_FAILURE = "失败";
        /**
         * 成功
         */
        public static final String MSG_SUCCESS = "成功";
        /**
         * 不允许为空
         */
        public static final String MSG_NOT_EMPTY = "不允许为空";
        /**
         * 系统异常
         */
        public static final String MSG_SYSTEM_EXCEPTION = "系统异常";

        /**
         * 入参时间格式不正确
         */
        public static final String MSG_DATE_INPUT_FORMAT_ERROR = "时间格式不正确";

		 /**
         * 权限不足
         */
        public static final String PERMISSION_DENIED_ERROR = "权限不足";

        /**
         * 暂无数据
         */
        public static final String MSG_NO_DATA = "暂无数据";
    }

}
