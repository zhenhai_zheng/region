package com.hai.dao;

import com.hai.domain.Region;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RegionDao {

    /**
     * 根据父类ID和等级编号获取行政区字典表
     * @param parentId 父类ID
     * @param level 等级编号
     * @return
     */
    List<Region> findListForRegion(@Param("parentId") Integer parentId, @Param("level") Integer level);

    /**
     * 获取所有省的名称
     * @return
     */
    List<String> selectProvince();

    /**
     * 通过省名称获取市名称
     * @param name
     * @return
     */
    List<String> selectShiByProvince(String name);

    /**
     * 通过市名称获取区名称
     * @param name
     * @return
     */
    List<String> selectQuByShi(String name);

}