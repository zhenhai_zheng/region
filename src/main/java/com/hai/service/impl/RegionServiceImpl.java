package com.hai.service.impl;


import com.hai.config.Constant;
import com.hai.dao.RegionDao;
import com.hai.domain.Region;
import com.hai.pojo.output.RegionOutput;
import com.hai.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <p>
 * 行政区字典表 服务实现类
 * </p>
 *
 * @author zhenhai.zheng
 * @since 2017年10月11日 09:26:28
 */
@Service
public class RegionServiceImpl implements RegionService {

    @Autowired
    private RegionDao regionDao;

    @Override
    public RegionOutput getListForRegion(Integer parentId, Integer level) {
        RegionOutput output = new RegionOutput();
        try{
            List<Region> list = regionDao.findListForRegion(parentId, level);
            if(list != null && list.size() > 0) {
                output.setCode(Constant.CodeConfig.CODE_SUCCESS);
                output.setMessage(Constant.MessageConfig.MSG_SUCCESS);
                output.setRegionList(list);
            } else {
                output.setCode(Constant.CodeConfig.CODE_NOT_FOUND_RESULT);
                output.setMessage(Constant.MessageConfig.MSG_NO_DATA);
                output.setRegionList(null);
            }
        } catch (Exception e) {
            output.setCode(Constant.CodeConfig.CODE_SYSTEM_EXCEPTION);
            output.setMessage(Constant.MessageConfig.MSG_SYSTEM_EXCEPTION);
            output.setRegionList(null);
        }

        return output;
    }
}
